output "do_public_ip" {
  value = digitalocean_droplet.test_demo.*.ipv4_address
  # value = "Instances: ${element(aws_instance.web.*.id, 0)}"
}
