provider "digitalocean" {}


#data "digitalocean_images" "get_image"{
#  filter {
#    key = "image"
#    values = ["5050geek.com-2018"]
#  }
#}


#Webservers that will have docker on them. (containers)
resource "digitalocean_droplet" "test_demo" {
  image      = var.image
  name       = var.name
  region     = var.region
  size       = var.size
  backups    = var.backups
  monitoring = var.monitoring
  count      = 4
  tags = [
    "demo-${count.index + 1}",
    "ubuntu",
    "wpclover",
    "name_of_company"
  ]
}

#Memcache that will deployed private ip space 
resource "digitalocean_droplet" "memcache" {
  image      = var.mem_image
  name       = var.mem_name
  region     = var.mem_region
  size       = var.mem_size
  backups    = var.mem_backups
  monitoring = var.mem_monitoring
  count      = 4
  tags = [
    "mem-${count.index + 1}",
    "ubuntu",
    "wpclover",
    "memcache_servers",
    "name_of_company"
  ]
}

resource "digitalocean_domain" "test_domain" {
  name = "clov.us"
  #  ip_address = digitalocean_droplet.test_demo.*.ipv4_address
}

resource "digitalocean_firewall" "test_demo" {
  name = "only-22-53-443"

  droplet_ids = digitalocean_droplet.test_demo.*.id

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["192.168.1.0/24", "2002:1:2::/48"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "3306"
    source_addresses = ["192.168.1.0/24", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

}


#This section is for loadbalancers
resource "digitalocean_loadbalancer" "wp-public-1" {
  name                     = "test_domain_lb-1"
  region                   = "nyc3"
  algorithm                = "least_connections"
  enable_backend_keepalive = "true"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 80
    target_protocol = "http"
  }

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"

    target_port     = 443
    target_protocol = "https"
  }

  healthcheck {
    port                     = 443
    protocol                 = "http"
    path                     = "/"
    check_interval_seconds   = 5
    response_timeout_seconds = 3
    unhealthy_threshold      = 2
    healthy_threshold        = 2
  }

  droplet_ids = digitalocean_droplet.test_demo.*.id
}

resource "digitalocean_loadbalancer" "wp-public-2" {
  name                     = "test_domain_lb-2"
  region                   = "nyc3"
  algorithm                = "least_connections"
  enable_backend_keepalive = "true"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 80
    target_protocol = "http"
  }

  forwarding_rule {
    entry_port     = 443
    entry_protocol = "https"

    target_port     = 443
    target_protocol = "https"
  }

  healthcheck {
    port                     = 443
    protocol                 = "http"
    path                     = "/"
    check_interval_seconds   = 5
    response_timeout_seconds = 3
    unhealthy_threshold      = 2
    healthy_threshold        = 2
  }

  droplet_ids = digitalocean_droplet.test_demo.*.id
}

# Database cluster mysql
#The link for info is found here https://developers.digitalocean.com/documentation/v2/#databases
#https://www.terraform.io/docs/providers/do/r/database_cluster.html

resource "digitalocean_database_cluster" "wpc" {
  name       = "wpc-mysql-cluster"
  engine     = "mysql"
  version    = "10"
  size       = "db-s-6vcpu-16gb"
  region     = "nyc3"
  node_count = 4
  #  private_network_uuid = 
  tags = ["production"]
}
